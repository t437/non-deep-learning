import numpy as np
import io
import cv2
import sys

'''
Detect torx screws on Vereinzelungsplatte (Vplatte) even if in different positions
'''
# Parameters
threshold_value = 105 # threshold value between 100 and 110 works well with daylight, 90 in lowlight condition
min_area = 2000 # minimum area of object in order to be highlighted / detected (250 night, 300/350 daylight)
max_area = 3000

# for coordinates calculation
size_of_image= 26.3 # width in cm of the field of view

CAM_X = 3280  # width
CAM_Y = 2464  # height

# for coordinates calculation
cm_to_pixel = size_of_image/3280.0

# cut region of the FOW (=Resion Of Interest - ROI)
Y1 = 630
Y2 = 1550
X1 = 1125
X2 = 2080

# import image
image = cv2.imread('./sample_images/r3280_2464.jpg')
# cut region of the FOW (=Resion Of Interest - ROI)
vplatte = image[Y1: Y2, X1: X2] # rows and then cols! Set above according to resolution
cv2.imwrite('vplatte.jpg',vplatte)
vplatte_gray = cv2.cvtColor(vplatte, cv2.COLOR_BGR2GRAY)
'''
find way to achieve optimal threshold value (value between 100 and 110 works well).
[retval, dst=cv2.threshold(src, thresh, maxval, type[, dst]) read documentation for further info]
convert all pixels to either white or black and then inverse the values (black bgd and objects in white)
'''
_, threshold = cv2.threshold(vplatte_gray, threshold_value, 255, cv2.THRESH_BINARY_INV)
cv2.imwrite('threshold.jpg',threshold)
    # Detect the torx screws
contours, _ = cv2.findContours(threshold, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
area_values = []
n_objects = 0
final_coordinates = []
for cnt in contours:
    (x, y, w, h) = cv2.boundingRect(cnt)
    # highlight all detected contours with rectangle:
    #cv2.rectangle(vplatte, (x, y), (x + w, y + h), (0, 255, 0), 3) #(0, 255, 0) is green
    # Calculate area of contours in order to differenciate them & ignore the noise (e.g. holes in the Vplatte)
    area = cv2.contourArea(cnt)

    # Distinguish torx screws
    if area >= min_area and area <= max_area: # 250 lowlight, 350 with better lighting
        cv2.putText(vplatte, str(area), (x, y), 1, 1, (255, 0, 0))
        area_values.append(area)
        n_objects += 1
        # calculate coordinates of object
        # min enclosing circle
        (x,y),radius = cv2.minEnclosingCircle(cnt)
        center = (int(x),int(y))
        radius = int(radius)
        cv2.circle(vplatte,center,radius,(0,0,255),2)
        '''
To simplify calculations and detection, the FOW is cut to only the vplatte. Hence, the detected objects are in the
vplatte coordinate system; point (0,0) is upper left corner of the vplatte, not of the whole FOW.
In order to find the actual coordinates in respect to the FOW (in pixels) we need to add the portion of FOW around
the vplatte that was removed (section "cut region of the FOW" in resolution selection switch)
         '''
        # adjust coordinates from vplatte to frame (full FOW)
        x = x + X1
        y = y + Y1
        #print('coordinates: ', x, ', ', y)
        x_cm = x * cm_to_pixel
        y_cm = y * cm_to_pixel
        coord = (x_cm, y_cm)
        final_coordinates.append(coord)
        #print('cm coordinates: ', x_cm, ', ', y_cm)
        #if n_objects == 8:
         #   print(final_coordinates)
          #  sys.exit()

#print(area_values)

while True:
    # this part only necessary for good visualization on the desktop when using the Raspberry:
    # cv2.namedWindow('Frame', cv2.WINDOW_NORMAL)
    # cv2.resizeWindow('Frame', 1000, 800)
    # cv2.namedWindow('Vplatte', cv2.WINDOW_NORMAL)
    # cv2.resizeWindow('Vplatte', 1000, 1000)
    # cv2.namedWindow('Threshold', cv2.WINDOW_NORMAL)
    # cv2.resizeWindow('Threshold', 1000, 1000)

    cv2.imshow("Frame", image)
    cv2.imshow("Vplatte", vplatte)
    cv2.imshow("Threshold", threshold)
    key = cv2.waitKey(1)
    if key == 27: #esc key
        #print('Found ', n_objects, 'objects!')
        break

cv2.destroyAllWindows()
