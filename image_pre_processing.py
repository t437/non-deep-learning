'''
Source: https://docs.opencv.org/4.5.0/d5/daf/tutorial_py_histogram_equalization.html

Pre processing of image to improve it's quality for image detection
'''

import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt

IMAGE_PATH = "/Users/fabiomotta/Documents/Thesis/App/sample_images/vplatte_test_preprocessing.jpg"

def theory_example(img_gray):
    hist, bins = np.histogram(img_gray.flatten(), 256, [0, 256])
    '''
    THEORY:
    CDF: Cumulative Distribution Function (CDF).
    This encodes the fraction of pixels with an intensity that is equal to or less than a specific value.
    If h is a histogram and C is a CDF, then h(i) indicates the number of pixels with intensity of i.
    '''
    cdf = hist.cumsum()
    cdf_normalized = cdf * float(hist.max()) / cdf.max()
    plt.plot(cdf_normalized, color = 'b')
    plt.hist(img_gray.flatten(), 256, [0, 256], color = 'r')
    plt.xlim([0, 256])
    plt.legend(('cdf', 'histogram'), loc = 'upper left')
    plt.show()

    # Find the minimum histogram value (excluding 0) and apply the histogram equalization equation:
    cdf_m = np.ma.masked_equal(cdf, 0)
    cdf_m = (cdf_m - cdf_m.min()) * 255/(cdf_m.max() - cdf_m.min())
    cdf = np.ma.filled(cdf_m, 0).astype('uint8')

    # Now we have the look-up table that gives us the information on what is the output pixel value 
    # for every input pixel value. So we just apply the transform:
    img2 = cdf[img_gray]

    # Calculate histogram and cdf of img2
    cdf2 = hist.cumsum()
    cdf_normalized_2 = cdf2 * float(hist.max()) / cdf2.max()
    plt.plot(cdf_normalized_2, color = 'b')
    plt.hist(img2.flatten(), 256, [0, 256], color = 'r')
    plt.xlim([0, 256])
    plt.legend(('cdf2', 'histogram2'), loc = 'upper left')
    plt.show()


def historgram_equalization(img_gray):
    equ = cv.equalizeHist(img_gray)
    res = np.hstack((img_gray, equ)) #stacking images side-by-side
    cv.imwrite('./output_images/vplatte_test_preprocessing_equalizedHist.jpg', res)


def clahe_processing(img_gray):
    # create a CLAHE object (Arguments are optional).
    clahe = cv.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
    clahe2 = cv.createCLAHE()
    cl1 = clahe.apply(img_gray)
    cl2 = clahe2.apply(img_gray)
    comparison = np.hstack((img_gray, cl1, cl2)) #stacking images side-by-side
    cv.imwrite('./output_images/vplatte_test_preprocessing_clahe.jpg', cl1)
    cv.imwrite('./output_images/vplatte_test_preprocessing_clahe_noparam.jpg', cl2)
    cv.imwrite('./output_images/vplatte_test_preprocessing_clahe_comparison.jpg', comparison)

 
def lab_processing(img):
    img = cv.imread(IMAGE_PATH, 1)

    #-----Converting image to LAB Color model----------------------------------- 
    lab= cv.cvtColor(img, cv.COLOR_BGR2LAB)
    cv.imwrite('./output_images/lab.jpg', lab)

    #-----Splitting the LAB image to different channels-------------------------
    l, a, b = cv.split(lab)
    cv.imwrite('./output_images/l_channel.jpg', l)
    cv.imwrite('./output_images/a_channel.jpg', a)
    cv.imwrite('./output_images/b_channel.jpg', b)
    
    #-----Applying CLAHE to L-channel-------------------------------------------
    clahe = cv.createCLAHE(clipLimit=3.0, tileGridSize=(8,8))
    cl = clahe.apply(l)
    cv.imwrite('./output_images/lab_clahe_output.jpg', cl)

    #-----Merge the CLAHE enhanced L-channel with the a and b channel-----------
    limg = cv.merge((cl,a,b))
    cv.imwrite('./output_images/limg.jpg', limg)

    #-----Converting image from LAB Color model to RGB model--------------------
    final = cv.cvtColor(limg, cv.COLOR_LAB2BGR)
    cv.imwrite('./output_images/final.jpg', final)


from builtins import input
import argparse
def brightness():
    # Read image given by user
    parser = argparse.ArgumentParser(description='Code for Changing the contrast and brightness of an image! tutorial.')
    parser.add_argument('--input', help = 'Path to input image.', default = IMAGE_PATH)
    args = parser.parse_args()
    image = cv.imread(cv.samples.findFile(args.input))
    if image is None:
        print('Could not open or find the image: ', args.input)
        exit(0)
    new_image = np.zeros(image.shape, image.dtype)

    alpha = 1.0 # Simple contrast control
    beta = 0    # Simple brightness control
    # Initialize values
    print(' Basic Linear Transforms ')
    print('-------------------------')
    try:
        alpha = float(input('* Enter the alpha value [1.0-3.0]: '))
        beta = int(input('* Enter the beta value [0-100]: '))
    except ValueError:
        print('Error, not a number')
    # Do the operation new_image(i,j) = alpha*image(i,j) + beta
    # Instead of these 'for' loops we could have used simply:
    # new_image = cv.convertScaleAbs(image, alpha=alpha, beta=beta)
    # but we wanted to show you how to access the pixels :)
    for y in range(image.shape[0]):
        for x in range(image.shape[1]):
            for c in range(image.shape[2]):
                new_image[y,x,c] = np.clip(alpha*image[y,x,c] + beta, 0, 255)
    cv.imshow('Original Image', image)
    cv.imshow('New Image', new_image)
    # Wait until user press some key
    cv.waitKey()

import math
def gamma_correction(img):
    gamma = 1.5
    lookUpTable = np.empty((1,256), np.uint8)
    for i in range(256):
        lookUpTable[0,i] = np.clip(pow(i / 255.0, gamma) * 255.0, 0, 255)
    res = cv.LUT(img, lookUpTable)
    comparison = np.hstack((img, res)) #stacking images side-by-side
    cv.imwrite('./output_images/gamma_correction0.jpg', comparison)


    # Idea of method 1 & 2 from here: https://stackoverflow.com/questions/61695773/how-to-set-the-best-value-for-gamma-correction
    # METHOD 1: RGB
    # convert img to gray
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    # compute gamma = log(mid*255)/log(mean)
    mid = 0.5
    mean = np.mean(gray)
    gamma = math.log(mid*255)/math.log(mean)
    print(gamma)
    # do gamma correction
    img_gamma1 = np.power(img, gamma).clip(0,255).astype(np.uint8)

    # METHOD 2: HSV (or other color spaces)
    # convert img to HSV
    hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)
    hue, sat, val = cv.split(hsv)
    # compute gamma = log(mid*255)/log(mean)
    mid = 0.5
    mean = np.mean(val)
    gamma = math.log(mid*255)/math.log(mean)
    print(gamma)
    # do gamma correction on value channel
    val_gamma = np.power(val, gamma).clip(0,255).astype(np.uint8)

    # combine new value channel with original hue and sat channels
    hsv_gamma = cv.merge([hue, sat, val_gamma])
    img_gamma2 = cv.cvtColor(hsv_gamma, cv.COLOR_HSV2BGR)

    # save results
    cv.imwrite('./output_images/gamma_correction1.jpg', img_gamma1)
    cv.imwrite('./output_images/gamma_correction2.jpg', img_gamma2)


def main():
    #Histogram  equalization  and  CLAHE  functions  in  the  OpenCV  2  library  
    # can  only  be  applied  to  greyscale  images!
    img = cv.imread(IMAGE_PATH)
    img_gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

    #theory_example(img_gray)

    # Actual histogra equalization:
    #historgram_equalization(img_gray) # not useful, makes the image worse

    #clahe_processing(img_gray) # good approach

    #lab_processing(img) # works well

    # brightness() # not very useful

    gamma_correction(img)


main()